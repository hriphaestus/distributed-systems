# Одговори

1. Анализа на кодот
    - Примерот прикажан од вежбата е конзолска апликација која креира сервер и клиент во зависност од параметарот кој се
   испраќа. Програмата прима 2 или 3 аргументи во зависност од целта на повикување. Доколку целта е да креираме UDP
   сервер, потребно е да испратиме 2 или 3 аргументи, а доколку целта е да стартуваме UDP клиент, потребно е да
   испратиме точно 3 аргументи. Бројот на аргументи е за 1 поголем од аргументите кои се од наш интерес затоа што при
   повикување на програмата од терминалниот емулатор, на прва позиција (како прв елемент во низата) секогаш се сместува
   името на програмата која ја извршуваме. Аргументот на втората позција треба да е client или server. Доколку се работи
   за клиент, тогаш потребен ни е и трет аргмент кој ќе ја преставува адресата на серверот со кој комуницираме.
   Доколку програмата ја повикуваме со цел да креира сервер, третиот аргумент не е задолжителен затоа што сокетот може
   да се врзи со одредена IP адреса од некоја мрежна картичка, или доколку не се испрати аргумент, може да се поврзи на
   0.0.0.0, што значи серверот ќе биде достапен од било која мрежна картичка на компјутерот.

    - Доколку креираме сервер со програмата, серверот слуша за порака испратена од некој клиент. Генеираме случен број и
    проверуваме каква вредност добил бројот. Доколку вредноста е 1, тогаш ја „примаме“ пораката, а доколку вредноста е 0
   сервер се однесува како да ја „исфрлил“ порката. Доколку вредноста на случајниот број е 1, тогаш враќаме порака до
   клиентот со должината на пораката која ја испратил клиентот.
   - Доколку повикаме клиент за да се поврзи на некој сервер испраќаме порака со содржина: „'This is another message“,
   поставуваме timeout и чекаме серверот да одговори. Доколку серверот не одговори во времето кое сме го дефинирале,
   прикажуваме грешка. Во спротивен случај ја прикажуваме пораката која ја вратил серверот. Ова се повторува се додека
   серверот не задоцни со одговор.
   - 
2. Зошто се користи connect кај UDP врска?
- connect се користи за поврзување на „далечински“ сокет на одредена адреса.

3. За што служи settimeout?
- settimeout се користи за да се постави време на прекинување на чекање за блокирачки операции. Вредноста е цел број кој означува време (во секунди) за прекин на чекање за операцијата.

4. Објаснувањето е во [`explanation.md`](explanation.md)
5. Објаснувањето е во [`explanation.md`](explanation.md)
