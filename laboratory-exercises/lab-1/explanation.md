# Објаснување

Одговорите на теоретските прашања се поставени во датотеката [`answers.md`](answers.md)

Кодот го поделив на 3 датотеки. Подоли објаснети се трите различни датотеки. Направив неколку измени на примерот од
документот за лабораториската вежба. Дел од промените се заради читливост на кодот, а дел се за да поправам грешка што
забележав во примерот.

Со цел да го задоволам барањето за да се поделат клиентот и серверот да се повикуваат од различна локација, скриптата ја
поделив на 2 дела: `client.py` и `server.py`.

## client_server_sample.py
Ова е примерот од документот за лабораториска вежба број 1. Тука направив неколку промени со цел подобро да го
организирам кодот.

## client.py
Освен поделбата, тука е и решението за задача 5 од лабораториската вежба. Поради тоа што скриптата работи со аргументи и
има потреба од неколку различни наредби, одлучив да ја користам библиотеката `argparse` која доаѓа од стандардбата
библиотека на Python. Имплементирав 3 подкоманди `tobits`, `tobytes`, `length`. За полесно преставување и читање на
податоците одлучив да користам `json` за комуникација помеѓу клиентот и серверот.

По извршување на програмата, се врши parsing на аргументите кои се испратени и се врши валдација на командите и
податокот кој е испратен. Потоа се креира нов UDP сокет и се испраќаат податоците од наредбата до серверот. Потоа се
очекува одговор од серверот и тој одговор се прикажува во терминалниот емулатор (конзола).

## server.py
Серверот истата така поддржува argument parsing преку `argparse`. Разликата е во тоа што тука се очекува само еден
аргумент `--interface` кој не е задолжителен. Доколку не се испрати аргументот `--interface`, тогаш серверот се поврзува
со `0.0.0.0`.

Се креира UDP сокет и се чека конекција од некој клиент. Кога ќе се поврзи некој клиент се очекува `json` порака која
потоа се испраќа на функцијата `handle_command`. Се пресметува одговорот во зависност од наредбата и одговорот се
испраќа на клиентот.
