from sys import getsizeof
import json
import socket
import argparse

MAX_DATA = 65535
SOCKET_PORT = 1060


def handle_command(data: dict) -> str:
    command = data.get('command')
    print('Command is:', command)

    if not command:
        return 'Failed to perform calculation because a command was not provided.'

    if command == 'tobits':
        return str(getsizeof(data.get('value')) * 8)
    elif command == 'tobytes':
        return str(getsizeof(data.get('value')))
    elif command == 'length':
        return str(len(data.get('value')))


def main() -> None:
    parser = argparse.ArgumentParser('UDP server')
    parser.add_argument('--interface', type=str, help='ip address for biding', default='0.0.0.0')

    args = parser.parse_args()

    s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
    s.bind((args.interface, SOCKET_PORT))

    print(f'Listening at {s.getsockname()}')

    while True:
        data, address = s.recvfrom(MAX_DATA)

        print('Got a connection from:', address)
        print('The data I got from', address, 'is', data)

        parsed_data = json.loads(data.decode())

        result = handle_command(parsed_data)
        print('The result is:', result)

        print('Sending result...\n___________________________\n')
        s.sendto(result.encode(), address)


if __name__ == '__main__':
    main()

