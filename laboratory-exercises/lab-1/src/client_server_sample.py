import sys
import socket
import random

MAX_DATA = 65535
SOCKET_PORT = 1060


def main(argv: list[str]) -> None:
    s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)

    if len(argv) < 2:
        print('Incorrect usage', file=sys.stderr)
        print('\nclient_server_sample.py client <host>', file=sys.stderr)
        print('client_server_sample.py server [<interface>]', file=sys.stderr)
        return

    if argv[1] == 'server':
        interface = argv[2] if len(argv) == 3 else '0.0.0.0'
        s.bind((interface, SOCKET_PORT))
        print(f'Listening at {s.getsockname()}')

        while True:
            data, address = s.recvfrom(MAX_DATA)
            if random.randint(0, 1):
                print(f'The client from {address} says: {repr(data)}')
                s.sendto(f'Your data was {len(data)}'.encode(), address)
            else:
                print(f'Pretending to drop packet from: {address}')
    elif argv[1] == 'client' and len(argv) == 3:
        hostname = argv[2]

        s.connect((hostname, SOCKET_PORT))
        print(f'Client socket name is: {s.getsockname()}')

        delay = 0.1
        while True:
            s.send('This is another message'.encode())

            print(f'Waiting up to {delay} seconds for the server to reply.')

            s.settimeout(delay)

            try:
                data = s.recv(MAX_DATA)

                print(f'The server says: {repr(data)}')
            except socket.timeout:
                delay *= 2
                if delay > 2.0:
                    raise TimeoutError('The server may be offline...')
    else:
        print('Incorrect usage', file=sys.stderr)
        print('In the case of a client socket, the host attribute is required', file=sys.stderr)
        print('\nclient_server_sample.py client <host>')
        print('client_server_sample.py server [<interface>]')


if __name__ == '__main__':
    main(sys.argv)

