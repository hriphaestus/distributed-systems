import json
import socket
import argparse

MAX_DATA = 65535
SOCKET_PORT = 1060


def main() -> None:
    parser = argparse.ArgumentParser('UDP client')
    parser.add_argument('host', type=str, help='an IP address of the host machine.')

    subparsers = parser.add_subparsers(title='Subcommands', required=True, dest='command')

    parser_tobits = subparsers.add_parser('tobits', help='receive a calculation in bits')
    parser_tobits.add_argument('value', type=int, help='number of bytes to convert')

    parser_tobytes = subparsers.add_parser('tobytes', help='receive a calculation in bytes')
    parser_tobytes.add_argument('value', type=int, help='number of bits to convert')

    parser_length = subparsers.add_parser('length', help='receive length of string in bytes')
    parser_length.add_argument('value', type=str, help='input string to calculate length')

    args = parser.parse_args()

    print(args)

    s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)

    print(f'Connecting to: {args.host}:{SOCKET_PORT}')
    s.connect((args.host, SOCKET_PORT))

    s.send(json.dumps({
        'command': args.command,
        'value': args.value
    }).encode())

    try:
        data = s.recv(MAX_DATA)
        print('\nServer:', data.decode(), sep='\n')
    except OSError:
        print('An error occurred while trying to receive response from the server.')
    finally:
        s.close()


if __name__ == '__main__':
    main()

