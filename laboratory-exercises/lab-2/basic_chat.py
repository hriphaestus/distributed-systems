from user.user import User


def main():
    hristijan = User('hristijan', 'Petreski', 'hristijan', 'pass123')
    hristijan.add_conversation('random')
    hristijan.add_message('Hello', 'random')
    print(hristijan.get_messages('random'))
    hristijan.add_conversation('random')
    try:
        hristijan.add_message('whatever', 'someone')
    except KeyError:
        print('Caught the error')

    print('All conversations', hristijan.conversations)


if __name__ == '__main__':
    main()

