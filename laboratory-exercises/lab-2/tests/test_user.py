import unittest
from user.user import User


class TestUser(unittest.TestCase):
    def test_add_conversation(self):
        user = User("John", "Doe", "jdoe", "password")
        user.add_conversation("jane")
        self.assertTrue("jane" in user.conversations)

    def test_add_message(self):
        user = User("John", "Doe", "jdoe", "password")
        user.add_conversation("jane")
        user.add_message("Hello Jane!", "jane")
        self.assertEqual(user.get_messages("jane"), ["Hello Jane!"])

    def test_add_message_invalid_user(self):
        user = User("John", "Doe", "jdoe", "password")
        with self.assertRaises(KeyError):
            user.add_message("Hello Jane!", "jane")

    def test_get_messages(self):
        user = User("John", "Doe", "jdoe", "password")
        user.add_conversation("jane")
        user.add_message("Hello Jane!", "jane")
        user.add_message("How are you?", "jane")
        self.assertEqual(user.get_messages("jane"), ["Hello Jane!", "How are you?"])


if __name__ == '__main__':
    unittest.main()
