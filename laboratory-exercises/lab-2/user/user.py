class User:
    def __init__(self, first_name, last_name, username, password):
        self.first_name = first_name
        self.last_name = last_name
        self.username = username
        self.password = password

        self.conversations: dict[str, list] = dict()

    def add_conversation(self, user: str):
        if self.conversations.get(user) is None:
            self.conversations[user] = []

    def add_message(self, message: str, user: str):
        messages: list = self.conversations.get(user)

        if messages is None:
            raise KeyError(f'Unable to find the user "{user}"')

        messages.append(message)

        self.conversations[user] = messages

    def get_messages(self, user: str):
        return self.conversations.get(user)

