import socket
import struct
import sys
import threading
from xmlrpc import client


def receive_all(sock: socket.socket, length):
    data = ''
    while len(data) < length:
        more = sock.recv(length - len(data)).decode()
        if not more:
            raise EOFError(f'Socket closed {len(data)} bytes into a {length} byte long message.')
        else:
            data += more

    return data.encode()


def wait(sock: socket.socket):
    sock.listen(2)

    while True:
        client_socket, address = sock.accept()
        length = struct.unpack("!i", receive_all(client_socket, 4))[0]

        data = receive_all(client_socket, length)
        data = data.decode().split('|')

        print(f'\n{data[0]} says: {data[1]}\n')
        client_socket.close()


main_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
main_socket.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)

proxy = client.ServerProxy('http://localhost:7001', allow_none=True)


def print_menu() -> None:
    print('---Zhu Chat---\n')
    print('[r] Register')
    print('[l] Login')
    print('[q] Logout')
    print('[c] Create Group')
    print('[j] Join Group')
    print('[f] Leave Group')
    print('[s] Send to User')
    print('[g] Send to Group\n')
    print('[e] Exit\n\n')


def main():
    while True:
        print_menu()
        command = input('-> ')

        if command == 'r':
            username = input('Enter your username: ')
            password = input('Enter your password: ')
            print(proxy.register(username, password))
        elif command == 'l':
            username = input('Enter your username: ')
            password = input('Enter your password: ')

            main_socket.bind(('0.0.0.0', 0))
            socket_info = main_socket.getsockname()
            print('Socket:', socket_info)
            threading.Thread(target=wait, args=[main_socket]).start()

            print(proxy.login(username, password, socket_info[0], str(socket_info[1])))
        elif command == 'q':
            print(proxy.logout(username))
        elif command == 'c':
            group_name = input('Enter group name: ')
            print(proxy.create_group(group_name))
        elif command == 'j':
            group_name = input('Enter group name: ')
            print(proxy.join_group(group_name, username, socket_info[0], socket_info[1]))
        elif command == 'f':
            group_name = input('Enter group name: ')
            print(proxy.leave_group(group_name, username))
        elif command == 's':
            recipient = input('Who is the recipient: ')
            message = input('Enter the message you would like to send\nMessage> ')

            response = proxy.send_to_user(username, recipient)

            if response != 'You are not logged in' and response != 'The recipient is not available':
                new_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
                new_socket.connect(response)

                message_text = f'{username}|{message}'

                new_socket.sendall(struct.pack("!i", len(message_text)) + message_text.encode())
                new_socket.close()
        elif command == 'g':
            group_name = input('Enter group name: ')
            message = input('Enter the message you would like to send\nMessage> ')

            response = proxy.send_to_group(group_name, message)

            if response != f'{group_name} doesn\'t exist' and response != 'You need to join the group':
                message_text = f'{username}|{message}'

                for user in response:
                    new_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
                    socket_info = response[user].split('|')
                    new_socket.connect((socket_info[0], int(socket_info[1])))
                    new_socket.sendall(struct.pack("!i", len(message_text)) + message_text.encode())
                    new_socket.close()
            else:
                print(response)
        elif command == 'e':
            main_socket.close()
            sys.exit(0)
        else:
            print('Unknown command')

        input('Press Enter to continue...')


if __name__ == '__main__':
    try:
        main()
    except KeyboardInterrupt:
        print('Finishing execution')
    except Exception as error:
        print('Something went wrong')
        print(error)
