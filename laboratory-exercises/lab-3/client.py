import socket
import struct
import threading
from xmlrpc.client import ServerProxy

main_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
main_socket.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)

proxy = ServerProxy('http://localhost:7001', allow_none=True)


def choose_type() -> str:
    print('Choose client type\n')
    print('[t] Teacher')
    print('[s] Student')

    while True:
        choice = input('> ')

        if choice == 't':
            user_type = 'teacher'
            break
        elif choice == 's':
            user_type = 'student'
            break
        else:
            print('Incorrect option')

    return user_type


def receive_all(sock: socket.socket, length):
    data = ''
    while len(data) < length:
        more = sock.recv(length - len(data)).decode()
        if not more:
            raise EOFError(f'Socket closed {len(data)} bytes into a {length} byte long message.')
        else:
            data += more

    return data.encode()


def teacher_socket(sock: socket.socket):
    sock.listen(2)

    while True:
        student, address = sock.accept()
        length = struct.unpack('!i', receive_all(student, 4))[0]

        data = receive_all(student, length)
        data = data.decode()

        print('This is the data I received:', data)
        # Convert received data to an array (ex. ['1','2','3'])
        parsed_data = [int(x.strip()[1:-1]) for x in data[1:-1].split(',')]

        # Find the absolute values and return a response
        transformed_data = [abs(x) for x in parsed_data]
        stringified_response = str(transformed_data)
        print('This is the transformed data:', transformed_data)
        student.sendall(struct.pack('!i', len(stringified_response)) + stringified_response.encode())
        student.close()


def handle_teacher():
    name = input('Enter a name: ')

    main_socket.bind(('0.0.0.0', 0))
    socket_info = main_socket.getsockname()
    print('Socket:', socket_info)

    response = proxy.register_teacher(name, socket_info[0], str(socket_info[1]))

    if response != 'A teacher with the same name has been registered. Try a different name':
        teacher_socket(main_socket)
    else:
        print(response)


def handle_student():
    name = input('Enter a name: ')
    raw_values = input('Enter array (comma separated): ')
    # Split and strip values
    values = [x.strip() for x in raw_values.split(',')]

    proxy.register_student(name)
    response = proxy.connect_to_teacher()

    if response != 'No available teachers':
        print(f'You are talking to {response[0]}')

        new_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        new_socket.connect((response[1], int(response[2])))

        stringified_values = str(values)

        new_socket.sendall(struct.pack('!i', len(stringified_values)) + stringified_values.encode())

        length = struct.unpack('!i', receive_all(new_socket, 4))[0]

        data = receive_all(new_socket, length)
        data = data.decode()

        print('The teacher responded:', data)
        new_socket.close()
    else:
        print(response)


def main():
    user_type = choose_type()

    if user_type == 'teacher':
        handle_teacher()
    else:
        handle_student()


if __name__ == '__main__':
    main()
