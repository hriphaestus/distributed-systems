from xmlrpc.server import SimpleXMLRPCServer


class User:
    def __init__(self, username, password, address=0, port=0, logged_in=False):
        self.username = username
        self.password = password
        self.address = address
        self.port = port
        self.logged_in = logged_in


class Group:
    def __init__(self, name):
        self.name = name
        self.users = {}


users: dict[str, User] = dict()
groups = dict()


def register(username, password):
    if username in users:
        return 'Username already taken'
    else:
        users[username] = User(username, password)
        return 'Registration successful'


def login(username, password, addresss, port):
    if username in users and users[username].password == password:
        users[username].logged_in = True
        users[username].address = addresss
        users[username].port = port
        return 'Login successful'
    else:
        return 'Incorrect username or password'


def logout(username):
    if username in users:
        del users[username]

        for group in groups:
            if username in groups[group].users:
                del groups[group].users[username]

        return 'Successful logout'
    else:
        return 'User is not logged in'


def create_group(name):
    if name in groups:
        return 'Group name already taken'
    else:
        groups[name] = Group(name)
        return 'Empty group successfully created'


def join_group(name, username, interface, port):
    if name in groups:
        if username in groups[name].users:
            return 'User is already in the group'
        else:
            groups[name].users = str(f'{interface}|${port}')
            return 'You were added to the group'


def leave_group(name, username):
    if name in groups:
        try:
            del groups[name].users[username]
            return 'Successfully removed from the group'
        except KeyError:
            return 'User not in group'


def send_to_user(sender, recipient):
    print(sender)

    if sender not in users or users[sender].logged_in is False:
        return 'You are not logged in'

    if recipient in users and users[recipient].logged_in is True:
        user_info = users[recipient]
        return user_info.address, user_info.port
    else:
        return 'The recipient is not available'


def send_to_group(name, username):
    if name not in groups:
        return f'{name} doesn\'t exist'

    if username in groups[name].users and users[username].logged_in:
        return groups[name].users
    else:
        return 'You need to join the group'


def main():
    server = SimpleXMLRPCServer(('localhost', 7001))
    server.register_introspection_functions()
    server.register_multicall_functions()

    server.register_function(register)
    server.register_function(login)
    server.register_function(logout)
    server.register_function(create_group)
    server.register_function(join_group)
    server.register_function(leave_group)
    server.register_function(send_to_user)
    server.register_function(send_to_group)

    print('Starting server...')
    server.serve_forever()


if __name__ == '__main__':
    main()
