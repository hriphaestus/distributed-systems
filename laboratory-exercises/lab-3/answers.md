# Одговори

Во оваа датотека одговорени се само кратките прашања. Остатокот од објаснувањето се наоѓа во
[`explanation.md`](explanation.md)

1.1 Излезот од примерот за пакување на податок е: 
```python
b'\x80\x04\x95!\x00\x00\x00\x00\x00\x00\x00]\x94(K\x01K\x02]\x94(\x8c\x04ime1\x94\x8c\x08prezime1\x94K}ee.'
```

1.2 Излезот од отпакувањето на сложениот податок е: 
```python
[1, 2, ['ime1', 'prezime1', 125]]
```