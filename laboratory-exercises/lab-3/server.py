from xmlrpc.server import SimpleXMLRPCServer


class User:
    def __init__(self, user_type, name):
        self.name = name
        self.user_type = user_type


class Teacher(User):
    def __init__(self, user_type, name, interface, port):
        super().__init__(user_type, name)
        self.interface = interface
        self.port = port


students: dict[str, User] = dict()
teachers: dict[str, Teacher] = dict()


def register_teacher(name: str, interface: str, port: str):
    if name in teachers:
        return 'A teacher with the same name has been registered. Try a different name'
    else:
        teachers[name] = Teacher('teacher', name, interface, port)
        return 'Successful registration'


def register_student(name):
    if name in students:
        return 'A student with the same name has been registered. Try a different name'
    else:
        students[name] = User('student', name)
        return 'Successful registration'


def connect_to_teacher() -> tuple | str:
    if len(teachers) > 0:
        teacher = list(teachers.keys())[0]
        print('This is the teacher:', teacher)

        return teachers[teacher].name, teachers[teacher].interface, teachers[teacher].port
    return 'No available teachers'


def main():
    server = SimpleXMLRPCServer(('localhost', 7001))
    server.register_introspection_functions()
    server.register_multicall_functions()

    server.register_function(register_teacher)
    server.register_function(register_student)
    server.register_function(connect_to_teacher)

    print('Starting server...')
    server.serve_forever()


if __name__ == '__main__':
    main()
